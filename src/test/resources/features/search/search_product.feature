### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

Feature: Search API
  As a user
  I want to search for products on the Waarkoop server
  So that I can find the products I need


  Scenario: Search test with available orange product
    When I call endpoint orange
    Then I see the results displayed for orange

  Scenario: Search test with available mango product
    When I call endpoint apple
    Then I see the results displayed for mango

  Scenario: Search test with available pasta product
    When I call endpoint pasta
    Then I see the results displayed for pasta

  Scenario: Search test with available cola product
    When I call endpoint cola
    Then I see the results displayed for cola

  Scenario: Search test with available car product
    When I call endpoint car
    Then I do not see the results

  Scenario: Not Found
    When I call endpoint car_one
    Then I see not found detail