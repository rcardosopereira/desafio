package starter.api;

import io.restassured.response.Response;
import static net.serenitybdd.rest.RestRequests.given;

public class SearchAPI {
    private final String BASE_URL = "https://waarkoop-server.herokuapp.com/api/v1/search/demo";

    public Response getOrange() {
        return given()
                .baseUri(BASE_URL)
                .basePath("/orange")
                .when()
                .get();
    }

    public Response getApple() {
        return given()
                .baseUri(BASE_URL)
                .basePath("/apple")
                .when()
                .get();
    }

    public Response getPasta() {
        return given()
                .baseUri(BASE_URL)
                .basePath("/pasta")
                .when()
                .get();
    }

    public Response getCola() {
        return given()
                .baseUri(BASE_URL)
                .basePath("/cola")
                .when()
                .get();
    }

    public Response getCar() {
        return given()
                .baseUri(BASE_URL)
                .basePath("/car")
                .when()
                .get();
    }

    public Response getNotFound() {
        return given()
                .baseUri(BASE_URL)
                .basePath("/car/1")
                .when()
                .get();
    }

}