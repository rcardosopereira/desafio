package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Test;
import starter.api.SearchAPI;
import java.util.List;
import static net.serenitybdd.rest.SerenityRest.then;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class SearchStepDefinitions {

    private SearchAPI searchAPI;
    private Response response;

    @Before
    public void setUp() {
        searchAPI = new SearchAPI();
    }

    @Test
    @When("I call endpoint orange")
    public void i_call_endpoint() {
        Response response = searchAPI.getOrange();
        response.then().statusCode(200);
    }

    @Then("I see the results displayed for orange")
    public void i_see_the_results_displayed_for_Orange() {
        Response response = searchAPI.getOrange();
        List<String> orangeList = response.jsonPath().getList("title");
        assertThat(orangeList, hasItem("Crystal Clear Orange Mandarin Pak 1,5L"));
        System.out.println("Here is the list of Items: " + orangeList);
    }

    @When("I call endpoint apple")
    public void i_call_endpoint_apple() {
        Response response = searchAPI.getApple();
        response.then().statusCode(200);
    }

    @Then("I see the results displayed for mango")
    public void he_see_the_results_displayed_for_mango() {
        restAssuredThat(response -> response.body("title", hasItem("I'm Juice Pineapple Melon Mango 500 ml")));
    }

    @When("I call endpoint pasta")
    public void i_call_endpoint_pasta() {
        Response response = searchAPI.getPasta();
        response.then().statusCode(200);
    }

    @Then("I see the results displayed for pasta")
    public void i_see_the_results_displayed_for_pasta() {
        restAssuredThat(response -> response.body("brand", hasItem("Superunie")));
    }

    @When("I call endpoint cola")
    public void i_call_endpoint_cola() {
        Response response = searchAPI.getCola();
        response.then().statusCode(200);
    }

    @Then("I see the results displayed for cola")
    public void i_see_the_results_displayed_for_cola() {
        restAssuredThat(response -> response.body("title", hasItem("Coca-cola Original taste 4 x 1,5 L")));
        Response response = searchAPI.getCola();
        response.then().statusCode(200);
    }


    @When("I call endpoint car")
    public void i_call_endpoint_car() {
        Response response = searchAPI.getCar();
        response.then().statusCode(404);
    }

    @Then("I do not see the results")
    public void i_do_not_see_the_results() {
        then().statusCode(404).body("detail.error", is(true));
        then().statusCode(404).body("detail.message", is("Not found"));
        then().statusCode(404).body("detail.requested_item", is("car"));
        then().statusCode(404).body("detail.served_by", is("https://waarkoop.com"));
    }

    @When("I call endpoint car_one")
    public void i_call_endpoint_car_one() {
        Response response = searchAPI.getNotFound();
        response.then().statusCode(404);
    }

    @Then("I see not found detail")
    public void i_see_not_found_detail() {
        then().statusCode(404).body("detail", is("Not Found"));
    }

}
